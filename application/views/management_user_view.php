<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        EDMS
    </title>
    <!-- Favicon -->
    <link href="<?php echo base_url('assets'); ?>/assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css"
        rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?php echo base_url('assets'); ?>/assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
</head>

<body class="">
    <!-- SIDEBAR -->
    <?php include 'template/sidebar.php'; ?>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- Navbar -->
        <?php include 'template/navbar.php'; ?>
        <!-- End Navbar -->
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">

                </div>
            </div>
        </div>
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row">
                                <?php if($this->session->flashdata('notif')): ?>
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <strong><?php echo $this->session->flashdata('notif'); ?></strong>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="col-8">
                                    <h3 class="mb-0">User data</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="<?php echo base_url('management_user/new'); ?>" class="btn btn-success">New
                                        User</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Password</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Departement</th>
                                        <th scope="col">Photo</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach($users->result_array() as $row): ?>
                                    <tr>
                                        <td scope="row"><?php echo $no; ?></td>
                                        <td scope="row"><?php echo $row['name']; ?></td>
                                        <td scope="row"><?php echo $row['username']; ?></td>
                                        <td scope="row">****************</td>
                                        <td scope="row"><?php echo $row['role']; ?></td>
                                        <td scope="row"><?php echo $row['departement_name']; ?></td>
                                        <td scope="row"><?php echo $row['photo']; ?></td>
                                        <td scope="row">
                                            <a href="<?php echo base_url('management_user/edit/'); ?><?php echo $row['user_id']; ?>"
                                                class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                            <a href="#" class="btn btn-danger delete" data-toggle="modal" data-target="#modal-notification" data-id="<?php echo $row['user_id']; ?>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer py-4">
                            <!-- 
                            <nav aria-label="...">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">
                                            <i class="fas fa-angle-left"></i>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li class="page-item active">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            <i class="fas fa-angle-right"></i>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer class="footer">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2019 <a href="http://multikompetensi.com" class="font-weight-bold ml-1"
                                target="_blank">Multi Kompetensi</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="http://multikompetensi.com" class="nav-link" target="_blank">Multi
                                    Kompetensi</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://multikompetensi.com/profil-multi-kompetensi/" class="nav-link"
                                    target="_blank">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!--   Core   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js">
    </script>
    <!--   Optional JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
    <!--   Argon JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/argon-dashboard.min.js?v=1.1.0"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
    <script>
    window.TrackJS &&
        TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
        });
    </script>
</body>

</html>


<!--Modal -->
<div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification"
    aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">

            <div class="modal-header">
                <h4 class="modal-title" id="modal-title-notification">Dialog Window</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="py-3 text-center">
                    <i class="ni ni-bell-55 ni-3x"></i>
                    <h4 class="heading mt-4">Confirmation!</h4>
                    <p>Are you sure want to delete this account?</p>
                </div>

            </div>

            <div class="modal-footer">
                <a href="" id="delete-link" class="btn btn-white">Ok</a>
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<script>
  $(document).on("click", ".delete", function () {
    var id = $(this).data('id');
    document.getElementById("delete-link").href="<?php echo base_url('management_user/delete/'); ?>"+id;
  });
  </script>