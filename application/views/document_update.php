<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        EDMS
    </title>
    <!-- Favicon -->
    <link href="<?php echo base_url('assets'); ?>/assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css"
        rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?php echo base_url('assets'); ?>/assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
    <style>
    .ck-editor__editable_inline {
        min-height: 400px;
    }

    ins {
        background: lightgreen;
        text-decoration: none;
    }

    del {
        background: pink;
    }
    </style>
</head>

<body class="">
    <!-- SIDEBAR -->
    <?php include 'template/sidebar.php'; ?>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- Navbar -->
        <?php include 'template/navbar.php'; ?>
        <!-- End Navbar -->
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">

                </div>
            </div>
        </div>
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card shadow">
                        <div class="card-header">
                            Update document
                        </div>
                        <div class="card-body">
                            <form
                                action="<?php echo base_url('documents/update/'); ?><?php echo $document['document_id']; ?>"
                                method="post" enctype="multipart/form-data">
                                <input type="hidden" name="revision" value="<?php echo $document['revision']; ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Document Code</label>
                                            <input type="text" name="code" class="form-control"
                                                value="<?php echo $document['code']; ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Document Name</label>
                                            <input type="text" name="document_name" class="form-control"
                                                value="<?php echo $document['document_name']; ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">File</label>
                                            <input type="file" name="file" id="file" class="form-control">
                                            <input type="hidden" name="file_old" value="<?php echo $document['file']; ?>">
                                            <br>
                                            Old File : <?php if($document['file'] == ''): ?>
                                            <span class="badge badge-pill badge-warning">Empty file</span>
                                            <?php else: ?>
                                            <a href="<?php echo base_url('uploads/documents/') ?>"><span
                                                    class="badge badge-pill badge-warning"><?php echo $document['file']; ?></span></a>
                                            <?php endif; ?><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Document Type</label>
                                            <select name="type" id="type" class="form-control" readonly>
                                                <option value="">Document Type</option>
                                                <?php foreach($document_types->result_array() as $doc_type): ?>
                                                <option value="<?php echo $doc_type['type_id']; ?>" <?php echo $doc_type['type_id'] == $document['type'] ? 'selected' : ''; ?>>
                                                    <?php echo $doc_type['type_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Author</label>
                                            <input type="hidden" class="form-control" name="author"
                                                value="<?php echo $document['author']; ?>">
                                            <input type="text" class="form-control" name="author_name"
                                                value="<?php echo $document['name']; ?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Departement</label>
                                            <select name="departement" id="departement" class="form-control" readonly>
                                                <option value="">Departement</option>
                                                <?php foreach($departements->result_array() as $dep): ?>
                                                <option value="<?php echo $dep['departement_id']; ?>" <?php echo $dep['departement_id'] == $document['departement'] ? 'selected' : ''; ?>>
                                                    <?php echo $dep['departement_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Content</label>
                                            <input type="hidden" name="content_old" value="<?php echo $document['content']; ?>">
                                            <textarea name="content_new" id="editor" cols="30" rows="10"
                                                class="form-control"><?php echo $document['content'] ?></textarea>
                                        </div>
                                        <button type="submit" name="btnSubmit"
                                            class="btn btn-primary btn-lg btn-block">Update Document</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer class="footer">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2019 <a href="http://multikompetensi.com" class="font-weight-bold ml-1"
                                target="_blank">Multi Kompetensi</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="http://multikompetensi.com" class="nav-link" target="_blank">Multi
                                    Kompetensi</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://multikompetensi.com/profil-multi-kompetensi/" class="nav-link"
                                    target="_blank">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!--   Core   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js">
    </script>
    <!--   Optional JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
    <!--   Argon JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/argon-dashboard.min.js?v=1.1.0"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
    <script src="<?php echo base_url('assets/ckeditor5'); ?>/ckeditor.js"></script>
    <script>
    window.TrackJS &&
        TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
        });
    </script>
    <script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    </script>
</body>

</html>