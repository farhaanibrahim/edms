<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        EDMS
    </title>
    <!-- Favicon -->
    <link href="<?php echo base_url('assets'); ?>/assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css"
        rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?php echo base_url('assets'); ?>/assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
</head>

<body class="">
    <!-- SIDEBAR -->
    <?php include 'template/sidebar.php'; ?>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- Navbar -->
        <?php include 'template/navbar.php'; ?>
        <!-- End Navbar -->
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">

                </div>
            </div>
        </div>
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row">
                                <div class="col-8">
                                    <h3 class="mb-0">Documents</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="<?php echo base_url('documents/new'); ?>" class="btn btn-success">New Document</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Document Name</th>
                                        <th scope="col">Code</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Departement</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Revision</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($documents->result_array() as $row): ?>
                                    <tr>
                                        <td scope="row"><?php echo $no; ?></td>
                                        <td scope="row"><?php echo $row['document_name']; ?></td>
                                        <td scope="row"><?php echo $row['code']; ?></td>
                                        <td scope="row"><?php echo $row['name']; ?></td>
                                        <td scope="row"><?php echo $row['departement_name']; ?></td>
                                        <td scope="row"><?php echo $row['created_at']; ?></td>
                                        <td scope="row"><?php echo $row['revision']; ?></td>
                                        <td scope="row">
                                            <?php if($row['status'] == 0): ?>
                                                <span class="badge badge-pill badge-warning">Document is being reviewed</span>
                                            <?php elseif($row['status'] == 2): ?>
                                                <span class="badge badge-pill badge-danger">Document rejected by reviewer, <br><br>waiting for action from modifier</span>
                                            <?php else: ?>
                                                <span class="badge badge-pill badge-success">Latest document</span>
                                            <?php endif; ?>
                                        </td>
                                        <td scope="row">
                                        <a href="#" class="btn btn-sm btn-success">Detail</a>
                                        <?php if($row['status'] == 1): ?>
                                            <a href="<?php echo base_url('documents/update/'); ?><?php echo $row['document_id']; ?>" class="btn btn-sm btn-primary">Update document</a>
                                        <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer py-4">
                            <!-- 
                            <nav aria-label="...">
                                <ul class="pagination justify-content-end mb-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1">
                                            <i class="fas fa-angle-left"></i>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                    <li class="page-item active">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">
                                            <i class="fas fa-angle-right"></i>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <footer class="footer">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2019 <a href="http://multikompetensi.com" class="font-weight-bold ml-1"
                                target="_blank">Multi Kompetensi</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="http://multikompetensi.com" class="nav-link" target="_blank">Multi
                                    Kompetensi</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://multikompetensi.com/profil-multi-kompetensi/" class="nav-link"
                                    target="_blank">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!--   Core   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js">
    </script>
    <!--   Optional JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
    <!--   Argon JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/argon-dashboard.min.js?v=1.1.0"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
    <script>
    window.TrackJS &&
        TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
        });
    </script>
    <script>
    OrdersChart = function() {
        var e, a, t = $("#chart-doc-stat");
        $('[name="ordersSelect"]');
        t.length && (e = t, a = new Chart(e, {
            type: "bar",
            options: {
                scales: {
                    yAxes: [{
                        gridLines: {
                            lineWidth: 1,
                            color: "#dfe2e6",
                            zeroLineColor: "#dfe2e6"
                        },
                        ticks: {
                            callback: function(e) {
                                if (!(e % 10)) return e
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(e, a) {
                            var t = a.datasets[e.datasetIndex].label || "",
                                o = e.yLabel,
                                n = "";
                            return 1 < a.datasets.length && (n +=
                                    '<span class="popover-body-label mr-auto">' + t + "</span>"),
                                n += '<span class="popover-body-value">' + o + "</span>"
                        }
                    }
                }
            },
            data: {
                labels: ["Panduan", "Prosedur", "Instruksi", "Formulir"],
                datasets: [{
                    label: "Document Statistics",
                    data: [100, 30, 20, 49]
                }]
            }
        }), e.data("chart", a))
    }()
    </script>
</body>

</html>