<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        EDMS
    </title>
    <!-- Favicon -->
    <link href="<?php echo base_url('assets'); ?>/assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
    <link href="<?php echo base_url('assets'); ?>/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css"
        rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?php echo base_url('assets'); ?>/assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
</head>

<body class="">
    <!-- SIDEBAR -->
    <?php include 'template/sidebar.php'; ?>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- Navbar -->
        <?php include 'template/navbar.php'; ?>
        <!-- End Navbar -->
        <!-- Header -->
        <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
            style="min-height: 600px; background-image: url(<?php echo base_url('uploads/avatar'); ?>/<?php echo $this->session->userdata('photo') ?>); background-size: cover; background-position: center top;">
            <!-- Mask -->
            <span class="mask bg-gradient-default opacity-8"></span>
            <!-- Header container -->
            <div class="container-fluid d-flex align-items-center">
                <div class="row">
                    <div class="col-lg-7 col-md-10">
                        <h1 class="display-2 text-white">Hello <?php echo $this->session->userdata('name'); ?></h1>
                        <p class="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made
                            with your work and manage your profile info.</p>
                        <a href="#!" class="btn btn-info">Edit profile</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl-4 order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">My account</h3>
                                </div>
                                <div class="col-4 text-right">

                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if($this->session->flashdata('notif')): ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <strong><?php echo $this->session->flashdata('notif'); ?></strong>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if($this->session->flashdata('update_success')): ?>
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    <strong><?php echo $this->session->flashdata('update_success'); ?></strong>
                                </div>
                            </div>
                            <?php endif; ?>
                            <form action="<?php echo base_url('profile/update/'); ?><?php echo $user['user_id']; ?>"
                                method="post" enctype="multipart/form-data">
                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-name">Name</label>
                                                <input type="text" id="input-username"
                                                    class="form-control form-control-alternative" name="name"
                                                    value="<?php echo $user['name']; ?>" value="lucky.jesse">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-username">Username</label>
                                                <input type="text" id="input-username"
                                                    class="form-control form-control-alternative" name="username"
                                                    value="<?php echo $user['username']; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-first-name">New
                                                    Password</label>
                                                <input type="password" name="new_password"
                                                    class="form-control form-control-alternative"
                                                    placeholder="************">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-first-name">Old
                                                    Password</label>
                                                <input type="password" name="old_password"
                                                    class="form-control form-control-alternative"
                                                    placeholder="************">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="input-email">Change Photo</label>
                                                <input type="file" name="photo"
                                                    class="form-control form-control-alternative">
                                                <input type="hidden" name="old_photo"
                                                    value="<?php echo $user['photo']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label"
                                                    for="input-last-name">Departement</label>
                                                <select name="departement" id="departement" class="form-control">
                                                    <?php foreach($departement->result_array() as $dep): ?>
                                                    <option value="<?php echo $dep['departement_id']; ?>"
                                                        <?php echo $dep['departement_id'] == $user['departement'] ? 'selected' : ''; ?>>
                                                        <?php echo $dep['departement_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <img src="<?php echo base_url('uploads/avatar'); ?>/<?php echo $this->session->userdata('photo'); ?>"
                                                class="rounded-circle" style="width: 50%;"><br><br>
                                            <button type="submit" name="btnSubmit"
                                                class="btn btn-primary btn-lg btn-block">Update Profile</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">My recent work</h3>
                                </div>
                                <div class="col-4 text-right">

                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Document Name</th>
                                            <th scope="col">Code</th>
                                            <th scope="col">Departement</th>
                                            <th scope="col">Revision</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; ?>
                                        <?php foreach($recent_work->result_array() as $row): ?>
                                        <tr>
                                            <td scope="row"><?php echo $no; ?></td>
                                            <td scope="row"><?php echo $row['document_name']; ?></td>
                                            <td scope="row"><?php echo $row['code']; ?></td>
                                            <td scope="row"><?php echo $row['departement_name']; ?></td>
                                            <td scope="row"><?php echo $row['revision']; ?></td>
                                            <td scope="row">
                                                <a href="<?php echo base_url('profile/detail_recent_work/'); ?><?php echo $row['process_id']; ?>" class="btn btn-sm btn-success bg-gradient-success">Details</a>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <footer class="footer">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2019 <a href="http://multikompetensi.com" class="font-weight-bold ml-1"
                                target="_blank">Multi Kompetensi</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="http://multikompetensi.com" class="nav-link" target="_blank">Multi
                                    Kompetensi</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://multikompetensi.com/profil-multi-kompetensi/" class="nav-link"
                                    target="_blank">About Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!--   Core   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js">
    </script>
    <!--   Optional JS   -->
    <!--   Argon JS   -->
    <script src="<?php echo base_url('assets'); ?>/assets/js/argon-dashboard.min.js?v=1.1.0"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
    <script>
    window.TrackJS &&
        TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
        });
    </script>
</body>

</html>