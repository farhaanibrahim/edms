<?php

class Document_type_model extends CI_Model
{
    public function get()
    {
        return $this->db->get('document_types');
    }

    public function get_where($data)
    {
        $this->db->where($data);
        return $this->db->get('document_types');
    }

    public function add($data)
    {
        $this->db->insert('document_types', $data);
    }

    public function update($condition, $data)
    {
        $this->db->where($condition);
        $this->db->set($data);
        $this->db->update('document_types');
    }

    public function delete($id)
    {
        $this->db->where('type_id', $id);
        $this->db->delete('document_types');
    }
}
