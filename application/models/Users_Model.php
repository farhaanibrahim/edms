<?php

class Users_Model extends CI_Model
{
    public function get()
    {
        $this->db->join('roles', 'roles.role_id = users.role');
        $this->db->join('departements', 'departements.departement_id = users.departement');
        return $this->db->get('users');
    }

    public function get_where($data)
    {
        $this->db->where($data);
        $this->db->join('roles', 'roles.role_id = users.role');
        $this->db->join('departements', 'departements.departement_id = users.departement');
        return $this->db->get('users');
    }

    public function add($data)
    {
        $this->db->insert('users', $data);
    }

    public function update($condition, $data)
    {
        $this->db->where($condition);
        $this->db->set($data);
        $this->db->update('users');
    }

    public function delete($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('users');
    }
}
