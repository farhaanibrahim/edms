<?php

class Departement_Model extends CI_Model
{
    public function get()
    {
        return $this->db->get('departements');
    }

    public function get_where($data)
    {
        $this->db->where($data);
        return $this->db->get('departements');
    }

    public function add($data)
    {
        $this->db->insert('departements', $data);
    }

    public function update($condition, $data)
    {
        $this->db->where($condition);
        $this->db->set($data);
        $this->db->update('departements');
    }

    public function delete($id)
    {
        $this->db->where('departement_id', $id);
        $this->db->delete('departements');
    }
}
