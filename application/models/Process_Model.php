<?php

class Process_Model extends CI_Model
{
    public function get()
    {
        $this->db->join('departements', 'departements.departement_id = process.departement');
        return $this->db->get('process');
    }

    public function get_where($data)
    {
        $this->db->where($data);
        $this->db->join('departements', 'departements.departement_id = process.departement');
        return $this->db->get('process');
    }

    public function add($data)
    {
        $this->db->insert('process', $data);
    }

    public function update($condition, $data)
    {
        $this->db->where($condition);
        $this->db->set($data);
        $this->db->update('process');
    }

    public function delete($id)
    {
        $this->db->where('process_id', $id);
        $this->db->delete('process');
    }
}
