<?php

class Document_Model extends CI_Model
{
    public function get()
    {
        $this->db->join('users', 'users.user_id = documents.author');
        $this->db->join('departements', 'departements.departement_id = documents.departement');
        $this->db->join('document_types', 'document_types.type_id = documents.type');
        return $this->db->get('documents');
    }

    public function get_where($data)
    {
        $this->db->where($data);
        $this->db->join('users', 'users.user_id = documents.author');
        $this->db->join('departements', 'departements.departement_id = documents.departement');
        $this->db->join('document_types', 'document_types.type_id = documents.type');
        return $this->db->get('documents');
    }

    public function add($data)
    {
        $this->db->insert('documents', $data);
        return $last_id = $this->db->insert_id();
    }

    public function update($condition, $data)
    {
        $this->db->where($condition);
        $this->db->set($data);
        $this->db->update('documents');
    }

    public function delete($id)
    {
        $this->db->where('document_id', $id);
        $this->db->delete('documents');
    }
}
