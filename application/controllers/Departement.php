<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departement extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Departement_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index()
    {
        $data['tittle'] = 'Departement';
        $data['departements']  =$this->Departement_Model->get();

        $this->load->view('departement_view', $data);
    }

    public function new()
    {
        if (isset($_POST['btnSubmit'])) {
            $data = array(
                'departement_id'   => '',
                'departement_name' => $this->input->post('departement_name')
            );

            $this->Departement_Model->add($data);
            $this->session->set_flashdata('notif', 'Successfully added data departement');
            redirect(base_url('departement'));
        } else {
            $data['tittle'] = 'Departement';
            $this->load->view('departement_create', $data);
        }
    }

    public function edit($id)
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $param1 = array(
                'departement_id'   => $id
            );

            $param2 = array(
                'departement_name' => $this->input->post('departement_name')
            );

            $this->Departement_Model->update($param1, $param2);
            $this->session->set_flashdata('notif', 'Successfully edited data departement');
            redirect(base_url('departement'));
        } else {
            $param = array('departement_id' => $id);
            $data['tittle'] = 'Departement';
            $data['departement'] = $this->Departement_Model->get_where($param)->row_array();

            $this->load->view('departement_edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Departement_Model->delete($id);

        $this->session->set_flashdata('notif', 'Successfully deleted data departement');

        redirect(base_url('departement'));
    }
}
