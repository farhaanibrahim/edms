<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_Type extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Document_Type_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index()
    {
        $data['tittle'] = 'Document Type';
        $data['document_types'] = $this->Document_Type_Model->get();

        $this->load->view('document_type_view', $data);
    }

    public function new()
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $data = array(
                'type_id'   => '',
                'type_name' => $this->input->post('type_name')
            );

            $this->Document_Type_Model->add($data);
            $this->session->set_flashdata('notif', 'Successfully added document type');
            redirect(base_url('document_type'));
        } else {
            $data['tittle'] = 'Document Type';
            
            $this->load->view('document_type_create', $data);
        }
    }

    public function edit($id)
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $param1 = array('type_id'=>$id);
            $param2 = array('type_name'=>$this->input->post('type_name'));
            
            $this->Document_Type_Model->update($param1, $param2);
            $this->session->set_flashdata('notif', 'Successfully edited data document type');

            redirect(base_url('document_type'));
        } else {
            $param = array(
                'type_id' => $id
            );
            $data['tittle'] = 'Document Type';
            $data['document_type'] = $this->Document_Type_Model->get_where($param)->row_array();

            $this->load->view('document_type_edit', $data);
        }
    }

    public function delete($id)
    {
        $this->Document_Type_Model->delete($id);

        $this->session->set_flashdata('notif', 'Successfully deleted document type');

        redirect(base_url('document_type'));
    }
}
