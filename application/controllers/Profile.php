<?php

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_Model');
        $this->load->model('Departement_Model');
        $this->load->model('Process_Model');
        $this->load->model('Document_Type_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index($username)
    {
        $param = array('username'=>$username);
        $param2 = array('proceed_by' => $this->session->userdata('user_id'));
        $data['tittle'] = 'Profile';
        $data['departement'] = $this->Departement_Model->get();
        $data['user'] = $this->Users_Model->get_where($param)->row_array();
        $data['recent_work'] = $this->Process_Model->get_where($param2);
        
        $this->load->view('profile_view', $data);
    }

    public function update($user_id)
    {
        $id = array('user_id'=>$user_id);
        $get_user = $this->Users_Model->get_where($id)->row_array();

        $old_password = md5($this->input->post('old_password'));
        
        if ($this->input->post('new_password') == '') 
        {
            $config['upload_path']          = './uploads/avatar/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo'))
            {
                $user_data = array(
                    'name'          => $this->input->post('name'),
                    'departement'   => $this->input->post('departement'),
                    'photo'         => $this->input->post('old_photo')
                );
                $this->Users_Model->update($id, $user_data);
                
                $session_data = array(
                    'user_id'       => $user_id,
                    'name'          => $this->input->post('name'),
                    'departement'   => $this->input->post('departement'),
                    'photo'         => $this->input->post('old_photo'),
                );
                $this->session->set_userdata($session_data);

                $this->session->set_flashdata('update_success', 'Your profile successfully updated');
                redirect(base_url('profile/'.$get_user['username']));
            }
            else
            {
                $user_data = array(
                    'name'          => $this->input->post('name'),
                    'departement'   => $this->input->post('departement'),
                    'photo'         => $this->upload->data('file_name')
                );

                unlink('./uploads/avatar/'.$this->input->post('old_photo'));
                $this->Users_Model->update($id, $user_data);

                $session_data = array(
                    'user_id'       => $user_id,
                    'name'          => $this->input->post('name'),
                    'departement'   => $this->input->post('departement'),
                    'photo'         => $this->upload->data('file_name'),
                );
                $this->session->set_userdata($session_data);

                $this->session->set_flashdata('update_success', 'Your profile successfully updated');
                redirect(base_url('profile/'.$get_user['username']));
            }
        } else {
            if ($old_password !== $get_user['password']) 
            {
                $this->session->set_flashdata('notif', "You old password doesn't match");
                redirect(base_url('profile/'.$get_user['username']));
            } else {
                $config['upload_path']          = './uploads/avatar/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1000000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('photo'))
                {
                    $user_data = array(
                        'name'          => $this->input->post('name'),
                        'password'      => md5($this->input->post('new_password')),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->input->post('old_photo')
                    );
                    $this->Users_Model->update($id, $user_data);

                    $session_data = array(
                        'user_id'       => $user_id,
                        'name'          => $this->input->post('name'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->input->post('old_photo'),
                    );
                    $this->session->set_userdata($session_data);

                    $this->session->set_flashdata('update_success', 'Your profile successfully updated');
                    redirect(base_url('profile/'.$get_user['username']));
                }
                else
                {
                    $user_data = array(
                        'name'          => $this->input->post('name'),
                        'password'      => md5($this->input->post('new_password')),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->upload->data('file_name')
                    );

                    unlink('./uploads/avatar/'.$this->input->post('old_photo'));
                    $this->Users_Model->update($id, $user_data);

                    $session_data = array(
                        'user_id'       => $user_id,
                        'name'          => $this->input->post('name'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->upload->data('file_name'),
                    );
                    $this->session->set_userdata($session_data);

                    $this->session->set_flashdata('update_success', 'Your profile successfully updated');
                    redirect(base_url('profile/'.$get_user['username']));
                }
            }
        }
    }

    public function detail_recent_work($process_id)
    {
        $param = array(
            'process_id' => $process_id
        );
        $data['tittle'] = 'Recent Change';
        $data['document'] = $this->Process_Model->get_where($param)->row_array();
        $data['user'] = $this->Users_Model;
        $data['departement'] = $this->Departement_Model;
        $data['document_type'] = $this->Document_Type_Model;

        $this->load->view('recent_work', $data);
    }
}