<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_Model');
        $this->load->model('Departement_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index()
    {
        $data['tittle'] = 'Management User';
        $data['users'] = $this->Users_Model->get();

        $this->load->view('management_user_view', $data);
    }

    public function new()
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $config['upload_path']          = './uploads/avatar/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 10000000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo'))
            {
                $this->session->set_flashdata('notif', $this->upload->display_errors());
                redirect(base_url('management_user/new'));
            }
            else
            {
                $data = array(
                    'user_id'       => '',
                    'name'          => $this->input->post('name'),
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'role'          => $this->input->post('role'),
                    'departement'   => $this->input->post('departement'),
                    'photo'         => $this->upload->data('file_name'),
                );

                $this->Users_Model->add($data);
                $this->session->set_flashdata('notif', 'Successfully created a new user');
                redirect(base_url('management_user'));
            }
        } else {
            $data['tittle'] = 'Management User';
            $data['departement'] = $this->Departement_Model->get();
            $data['roles'] = $this->db->get('roles');
            $this->load->view('management_user_create', $data);
        }
    }

    public function edit($user_id)
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $id = array('user_id' => $user_id);
            $config['upload_path']          = './uploads/avatar/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;

            $this->load->library('upload', $config);

            if ($_POST['password'] == '') {
                if ( ! $this->upload->do_upload('photo'))
                {
                    $data = array(
                        'name'          => $this->input->post('name'),
                        'username'      => $this->input->post('username'),
                        'password'      => $this->input->post('old_password'),
                        'role'          => $this->input->post('role'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->input->post('old_photo'),
                    );

                    $this->Users_Model->update($id, $data);
                    $this->session->set_flashdata('notif', 'Successfully updated user');
                    redirect(base_url('management_user'));
                }
                else
                {
                    $data = array(
                        'name'          => $this->input->post('name'),
                        'username'      => $this->input->post('username'),
                        'password'      => $this->input->post('old_password'),
                        'role'          => $this->input->post('role'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->upload->data('file_name'),
                    );

                    $this->Users_Model->update($id, $data);
                    unlink('./uploads/avatar/'.$this->input->post('old_photo'));
                    $this->session->set_flashdata('notif', 'Successfully updated user');
                    redirect(base_url('management_user'));
                }
            } else {
                if ( ! $this->upload->do_upload('photo'))
                {
                    $data = array(
                        'name'          => $this->input->post('name'),
                        'username'      => $this->input->post('username'),
                        'password'      => $this->input->post('password'),
                        'role'          => $this->input->post('role'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->input->post('old_photo'),
                    );

                    $this->Users_Model->update($id, $data);
                    $this->session->set_flashdata('notif', 'Successfully updated user');
                    redirect(base_url('management_user'));
                }
                else
                {
                    $data = array(
                        'name'          => $this->input->post('name'),
                        'username'      => $this->input->post('username'),
                        'password'      => $this->input->post('password'),
                        'role'          => $this->input->post('role'),
                        'departement'   => $this->input->post('departement'),
                        'photo'         => $this->upload->data('file_name'),
                    );

                    $this->Users_Model->update($id, $data);
                    unlink('./uploads/avatar/'.$this->input->post('old_photo'));
                    $this->session->set_flashdata('notif', 'Successfully updated user');
                    redirect(base_url('management_user'));
                }
            }
        } else {
            $param = array('user_id'=>$user_id);
            $data['tittle'] = 'Management User';
            $data['roles'] = $this->db->get('roles');
            $data['departement'] = $this->Departement_Model->get();
            $data['user'] = $this->Users_Model->get_where($param)->row_array();
            
            $this->load->view('management_user_edit', $data);
        }
    }

    public function delete($user_id)
    {
        $id = array('user_id'=>$user_id);
        $user = $this->Users_Model->get_where($id)->row_array();

        $user_photo = $user['photo'];
        
        $this->Users_Model->delete($user_id);

        unlink('./uploads/avatar/'.$user_photo);

        $this->session->set_flashdata('notif', $user['name']."'s account deleted");
        redirect(base_url('management_user'));
    }
}