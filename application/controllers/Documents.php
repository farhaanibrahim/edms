<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Document_Model');
        $this->load->model('Document_Type_Model');
        $this->load->model('Departement_Model');
        $this->load->model('Process_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index()
    {
        $data['tittle'] = 'Documents';
        $data['documents'] = $this->Document_Model->get();
        $this->load->view('documents_view', $data);
    }

    public function new()
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $config['upload_path']          = './uploads/documents/';
            $config['allowed_types']        = 'doc|ppt|xls|docx|pptx|xlsx|pdf';
            $config['max_size']             = 1024000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                $data = array(
                    'document_id'   => '',
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content'       => $this->input->post('content'),
                    'file'          => '',
                    'revision'      => 0,
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'created_at'    => date('Ymd'),
                    'updated_at'    => ''
                );

                $document_id = $this->Document_Model->add($data);

                $data_for_reviewer = array(
                    'process_id'    => '',
                    'document_id'   => $document_id,
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content_old'   => $this->input->post('content'),
                    'content_new'   => $this->input->post('content'),
                    'file_old'      => '',
                    'file_new'      => '',
                    'revision'      => 0,
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'proceed_at'    => date('Ymd'),
                    'proceed_by'    => $this->session->userdata('user_id')
                );

                $this->Process_Model->add($data_for_reviewer);

                $this->session->set_flashdata('notif', 'Successfully created new document.\n Waiting to be reviewed and published,');
                redirect(base_url('documents'));
            }
            else
            {
                $data = array(
                    'document_id'   => '',
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content'       => $this->input->post('content'),
                    'file'          => $this->upload->data('file_name'),
                    'revision'      => 0,
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'created_at'    => date('Ymd'),
                    'updated_at'    => ''
                );

                $document_id = $this->Document_Model->add($data);

                $data_for_reviewer = array(
                    'process_id'    => '',
                    'document_id'   => $document_id,
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content_old'   => $this->input->post('content'),
                    'content_new'   => $this->input->post('content'),
                    'file_old'      => $this->upload->data('file_name'),
                    'file_new'      => $this->upload->data('file_name'),
                    'revision'      => 0,
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'proceed_at'    => date('Ymd'),
                    'proceed_by'    => $this->session->userdata('user_id')
                );

                $this->Process_Model->add($data_for_reviewer);

                $this->session->set_flashdata('notif', 'Successfully created new document.\n Waiting to be reviewed and published,');
                redirect(base_url('documents'));
            }
        } else {
            $data['tittle'] = 'Create Document';
            $data['document_types'] = $this->Document_Type_Model->get();
            $data['departements'] = $this->Departement_Model->get();

            $this->load->view('document_create', $data);
        }
    }

    public function update($document_id)
    {
        if (isset($_POST['btnSubmit'])) 
        {
            $config['upload_path']          = './uploads/documents/';
            $config['allowed_types']        = 'doc|ppt|xls|docx|pptx|xlsx|pdf';
            $config['max_size']             = 1024000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                $data_for_reviewer = array(
                    'process_id'    => '',
                    'document_id'   => $document_id,
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content_old'   => $this->input->post('content_old'),
                    'content_new'   => $this->input->post('content_new'),
                    'file_old'      => $this->input->post('file_old'),
                    'file_new'      => '',
                    'revision'      => $this->input->post('revision'),
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'proceed_at'    => date('Ymd'),
                    'proceed_by'    => $this->session->userdata('user_id')
                );

                $this->Process_Model->add($data_for_reviewer);

                $doc_id = array(
                    'document_id'=>$document_id
                );
                $data_doc = array(
                    'status'  => 0
                );
        
                $this->Document_Model->update($doc_id, $data_doc);

                $this->session->set_flashdata('notif', 'Successfully created new document.\n Waiting to be reviewed and published,');
                redirect(base_url('documents'));
            }
            else
            {
                $data_for_reviewer = array(
                    'process_id'    => '',
                    'document_id'   => $document_id,
                    'document_name' => $this->input->post('document_name'),
                    'code'          => $this->input->post('code'),
                    'content_old'   => $this->input->post('content_old'),
                    'content_new'   => $this->input->post('content_new'),
                    'file_old'      => $this->input->post('file_old'),
                    'file_new'      => $this->upload->data('file_name'),
                    'revision'      => $this->input->post('revision'),
                    'status'        => 0,
                    'type'          => $this->input->post('type'),
                    'author'        => $this->input->post('author'),
                    'departement'   => $this->input->post('departement'),
                    'proceed_at'    => date('Ymd'),
                    'proceed_by'    => $this->session->userdata('user_id')
                );

                $this->Process_Model->add($data_for_reviewer);

                $doc_id = array(
                    'document_id'=>$document_id
                );
                $data_doc = array(
                    'status'  => 0
                );
        
                $this->Document_Model->update($doc_id, $data_doc);

                $this->session->set_flashdata('notif', 'Successfully created new document.\n Waiting to be reviewed and published,');
                redirect(base_url('documents'));
            }
        } else {
            $param = array('document_id' => $document_id);
            $data['tittle'] = 'Update document';
            $data['document_types'] = $this->Document_Type_Model->get();
            $data['departements'] = $this->Departement_Model->get();
            $data['document'] = $this->Document_Model->get_where($param)->row_array();

            $this->load->view('document_update', $data);
        }
    }
}