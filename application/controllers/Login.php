<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Users_Model');
    }

    public function index()
    {
        $this->load->view('login_page');
    }

    public function process()
    {
        $data = array(
            'username'  => $this->input->post('username'),
            'password'  => md5($this->input->post('password'))
        );

        $get_user = $this->Users_Model->get_where($data);

        if ($get_user->num_rows() == 1) 
        {
            $data = $get_user->row_array();
            $session_data = array(
                'user_id'       => $data['user_id'],
                'name'          => $data['name'],
                'username'      => $data['username'],
                'role'          => $data['role'],
                'departement'   => $data['departement'],
                'photo'         => $data['photo'],
                'login'         => TRUE
            );
            $this->session->set_userdata($session_data);
            redirect(base_url('dashboard'));
        } else {
            $this->session->set_flashdata('notif', 'Wrong username or password!');
            redirect(base_url('login'));
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('notif', 'You were logged out');
        redirect(base_url('login'));
    }
}

