<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Document_Model');
        $this->load->model('Process_Model');
        $this->load->model('Users_Model');
        $this->load->model('Departement_Model');
        $this->load->model('Document_Type_Model');
        if ($this->session->userdata('login') != TRUE) 
		{
			$this->session->set_flashdata('notif', 'Your session has expired');
			redirect(base_url('login'));
		}
    }

    public function index()
    {
        $param = array('status' => 0);
        $data['tittle'] = 'Review';
        $data['documents'] = $this->Process_Model->get_where($param);
        $data['user'] = $this->Users_Model;
        $data['departement'] = $this->Departement_Model;
        
        $this->load->view('review_view', $data);
    }

    public function detail($process_id)
    {
        $param = array(
            'process_id' => $process_id
        );
        $data['tittle'] = 'Review';
        $data['document'] = $this->Process_Model->get_where($param)->row_array();
        $data['user'] = $this->Users_Model;
        $data['departement'] = $this->Departement_Model;
        $data['document_type'] = $this->Document_Type_Model;

        $this->load->view('review_detail', $data);
    }

    public function reject($process_id)
    {
        $process_id = array(
            'process_id' => $process_id
        );

        $get_process_info = $this->Process_Model->get_where($process_id)->row_array();

        $document_id = array(
            'document_id'=>$get_process_info['document_id']
        );
        $data_doc = array(
            'status'  => 2
        );

        $this->Document_Model->update($document_id, $data_doc);
        
        $data_process = array(
            'status'=>1
        );

        $this->Process_Model->update($process_id, $data_process);

        $this->session->set_flashdata('notif', 'Revision rejected');
        redirect(base_url('review'));
    }

    public function accept($process_id)
    {
        $process_id = array(
            'process_id' => $process_id
        );

        $get_process_info = $this->Process_Model->get_where($process_id)->row_array();

        $document_id = array(
            'document_id'=>$get_process_info['document_id']
        );
        $data_doc = array(
            'content' => $get_process_info['content_new'],
            'file'    => $get_process_info['file_new'],
            'revision'=> $get_process_info['revision']+1,
            'status'  => 1,
            'updated_at' => date('Ymd')
        );

        $this->Document_Model->update($document_id, $data_doc);
        
        $data_process = array(
            'status'=>2
        );

        $this->Process_Model->update($process_id, $data_process);

        $this->session->set_flashdata('notif', 'Revision accepted');
        redirect(base_url('review'));
    }
}
