-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 12:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_edms`
--

-- --------------------------------------------------------

--
-- Table structure for table `departements`
--

CREATE TABLE `departements` (
  `departement_id` int(30) NOT NULL,
  `departement_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departements`
--

INSERT INTO `departements` (`departement_id`, `departement_name`) VALUES
(1, 'Sales'),
(2, 'Marketing'),
(3, 'Finance'),
(4, 'Human Resources'),
(5, 'Purchasing'),
(6, 'Administration'),
(7, 'Information System');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `document_id` int(30) NOT NULL,
  `document_name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `file` varchar(100) NOT NULL,
  `revision` int(30) NOT NULL,
  `status` int(30) NOT NULL,
  `type` int(30) NOT NULL,
  `author` int(30) NOT NULL,
  `departement` int(30) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`document_id`, `document_name`, `code`, `content`, `file`, `revision`, `status`, `type`, `author`, `departement`, `created_at`, `updated_at`) VALUES
(1, 'Prosedur', 'P001', '<h2>Tittle</h2><p>Content</p><p>Content</p>', '', 2, 2, 1, 1, 7, '2019-11-19', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `type_id` int(30) NOT NULL,
  `type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`type_id`, `type_name`) VALUES
(1, 'Panduan'),
(2, 'Prosedur'),
(3, 'Instruksi'),
(4, 'Formulir');

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `history_id` int(30) NOT NULL,
  `document_id` int(30) NOT NULL,
  `document_name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `old_content` mediumtext NOT NULL,
  `new_content` mediumtext NOT NULL,
  `old_file` varchar(100) NOT NULL,
  `new_file` varchar(100) NOT NULL,
  `revision` int(30) NOT NULL,
  `type` int(30) NOT NULL,
  `departement` int(30) NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `process`
--

CREATE TABLE `process` (
  `process_id` int(30) NOT NULL,
  `document_id` int(30) NOT NULL,
  `document_name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `content_old` mediumtext NOT NULL,
  `content_new` mediumtext NOT NULL,
  `file_old` varchar(100) NOT NULL,
  `file_new` varchar(100) NOT NULL,
  `revision` int(30) NOT NULL,
  `status` int(1) NOT NULL,
  `type` int(30) NOT NULL,
  `author` int(30) NOT NULL,
  `departement` int(30) NOT NULL,
  `proceed_at` date NOT NULL,
  `proceed_by` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `process`
--

INSERT INTO `process` (`process_id`, `document_id`, `document_name`, `code`, `content_old`, `content_new`, `file_old`, `file_new`, `revision`, `status`, `type`, `author`, `departement`, `proceed_at`, `proceed_by`) VALUES
(1, 1, 'Prosedur', 'P001', '', '<h2>Tittle</h2><p>Content</p>', '', '', 0, 2, 1, 1, 7, '2019-11-19', 1),
(2, 1, 'Prosedur', 'P001', '', '<h2>Tittle</h2><p>Content</p><p>Content</p>', '', '', 1, 2, 1, 1, 7, '2019-11-20', 1),
(5, 1, 'Prosedur', 'P001', '<h2>Tittle</h2><p>Content</p><p>Content</p>', '<h2>Tittle</h2><p>Content</p><p>Cont</p>', '', '', 2, 1, 1, 1, 7, '2019-11-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(30) NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Admin'),
(3, 'Manager'),
(4, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `role` int(30) NOT NULL,
  `departement` int(3) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `username`, `password`, `role`, `departement`, `photo`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 7, 'user.png'),
(2, 'Muhammad Farhan Ibrahim', 'farhaanibrahim', '6cfa9279712766d5c79379672021dbfc', 1, 7, 'Mk-small.png'),
(3, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 0, 0, '011104500_1568015992-up6.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departements`
--
ALTER TABLE `departements`
  ADD PRIMARY KEY (`departement_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`process_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departements`
--
ALTER TABLE `departements`
  MODIFY `departement_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `document_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `type_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `history_id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `process`
--
ALTER TABLE `process`
  MODIFY `process_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
